use std::path::Path;
use std::process::{exit, Command, Stdio};

#[path = "../lib/mod.rs"]
mod lib;
use lib::common::{confirm_action, get_paths_for_session, is_valid_name};

pub fn delete_session(name: String) {
    if !is_valid_name(name.to_string()) {
        println!("The session name \"{}\" is not valid", name);
        exit(1);
    }

    let docker_image_name = format!("dx11-{}", name);
    let paths = get_paths_for_session(name.to_string());

    if !Path::new(&paths.base_path).is_dir() {
        println!("There is no such session: {}", name);
        exit(1);
    }

    // Docker image
    if !confirm_action("Do you want to remove the Docker image?", false) {
        exit(0);
    }

    let mut docker_image_remove = Command::new("sudo");
    docker_image_remove.args(&["docker", "image", "remove", &docker_image_name]);

    docker_image_remove.stdin(Stdio::null());
    docker_image_remove.stdout(Stdio::inherit());
    docker_image_remove.stderr(Stdio::inherit());

    match docker_image_remove.output() {
        Err(err) => {
            println!("Error: Failed to remove the image ({})", err);
            exit(1);
        }
        _ => {}
    };

    // Session data
    if !confirm_action(
        "Do you really want to delete the session's data files?\nThis action cannot be undone",
        false,
    ) {
        exit(0);
    }

    println!("Removing {}", name);

    let mut rm_rf_session = Command::new("sudo");
    rm_rf_session.args(&["rm", "-Rf", paths.base_path.to_str().unwrap()]);

    rm_rf_session.stdin(Stdio::null());
    rm_rf_session.stdout(Stdio::inherit());
    rm_rf_session.stderr(Stdio::inherit());

    match rm_rf_session.output() {
        Err(err) => {
            println!("Error: Failed to remove the session ({})", err);
            exit(1);
        }
        _ => {}
    };
}
