use std::fs;
use std::path::Path;
use std::process::{exit, Command, Stdio};

#[path = "../lib/mod.rs"]
mod lib;
use lib::common::{confirm_action, ensure_session_paths, get_paths_for_session, is_valid_name};
use lib::config::{read_config, write_config, DX11Config};

pub fn new_session(name: &str, docker_file: &str, encrypt: bool, use_tor: bool) {
    if !is_valid_name(name.to_string()) {
        println!("The session name \"{}\" is not valid", name);
        exit(1);
    }
    let docker_file_path = Path::new(docker_file);
    if !docker_file_path.exists() {
        println!("Error: \"{}\" does not exist", docker_file);
        exit(1);
    } else if !docker_file_path.is_file() {
        println!("Error: \"{}\" cannot be a directory", docker_file);
        exit(1);
    }

    let paths = get_paths_for_session(name.to_string());

    if paths.config_file.exists() {
        if !confirm_action(
            "Warning: A session with this name already exists.\nDo you want to update it?",
            false,
        ) {
            println!("Skipping");
            exit(0);
        }

        let mut config = match read_config(&paths.config_file) {
            Err(err) => {
                println!("Error: Could not read the config file ({})", err);
                exit(1);
            }
            Ok(value) => value,
        };
        if config.encryptHome != encrypt {
            println!(
                "Warning: The encryption status cannot be changed {}",
                if config.encryptHome {
                    "(enabled)"
                } else {
                    "(disabled)"
                }
            );
        }
        config.useTor = use_tor;
        match write_config(&config, &paths.config_file) {
            Err(err) => {
                println!("Error: \"{}\" cannot update the settings", err);
                exit(1);
            }
            _ => {}
        }
    } else {
        let config = DX11Config::new(encrypt, use_tor);
        match write_config(&config, &paths.config_file) {
            Err(err) => {
                println!("Error: \"{}\" cannot store the settings", err);
                exit(1);
            }
            _ => {}
        }
    }

    // TODO: CHECK CURRENT SETUP

    if paths.docker_folder.exists() {
        match fs::remove_dir_all(&paths.docker_folder) {
            Err(err) => {
                println!(
                    "Error: \"{}\" unable to clean the current Docker cache data",
                    err
                );
                exit(1);
            }
            _ => {}
        }
    }

    match ensure_session_paths(name.to_string()) {
        Err(err) => {
            println!("Error: \"{}\" unable to create the session paths", err);
            exit(1);
        }
        _ => {}
    }

    // COPY DOCKER CACHE FILES RECURSIVELY
    if docker_file_path.parent().unwrap().display().to_string() == "" {
        // cp -a . => config.docker_file_path
        match copy_rec(Path::new("."), &paths.docker_folder) {
            Err(err) => {
                println!("Error: \"{}\" unable to copy the Docker cache data", err);
                exit(1);
            }
            _ => {}
        }
    } else {
        // cp -a docer_file_path.parent() => config.docker_file_path
        match copy_rec(
            docker_file_path.parent().as_ref().unwrap(),
            &paths.docker_folder,
        ) {
            Err(err) => {
                println!("Error: \"{}\" unable to copy the Docker cache data", err);
                exit(1);
            }
            _ => {}
        }
    }

    let mut needs_rename = false;
    if docker_file_path.file_name().unwrap() != "Dockerfile" {
        needs_rename = true;
    } else {
        match docker_file_path.extension() {
            Some(ext) => {
                if ext != "" {
                    needs_rename = true;
                }
            }
            _ => {}
        }
    }

    // Ensure that the cached Dockerfile exists on a canonical path
    if needs_rename {
        // mv <pref>/my-docker.file <pref>/Dockerfile
        match std::fs::rename(
            Path::new(
                &paths
                    .docker_folder
                    .join(docker_file_path.file_name().unwrap()),
            ),
            Path::new(&paths.docker_folder.join("Dockerfile")),
        ) {
            Err(err) => {
                println!("Error: Failed to build the image ({})", err);
                exit(1);
            }
            _ => {}
        }
    }

    let docker_image_name = format!("dx11-{}", name);
    let cached_docker_file_path = paths.docker_folder.join("Dockerfile");
    let build_command = "sudo";
    let build_command_args = [
        "docker",
        "build",
        "-t",
        &docker_image_name,
        "-f",
        &cached_docker_file_path.display().to_string(),
        &paths.docker_folder.display().to_string(),
    ];

    println!("Building the Docker image {}", &docker_image_name);

    let mut docker_build = Command::new(build_command);

    docker_build.stdin(Stdio::null());
    docker_build.stdout(Stdio::inherit());
    docker_build.stderr(Stdio::inherit());

    for arg in build_command_args.iter() {
        docker_build.arg(arg);
    }
    match docker_build.output() {
        Err(err) => {
            println!("Error: Failed to build the image ({})", err);
            exit(1);
        }
        Ok(_) => println!("Done"),
    };
}

fn copy_rec(from: &Path, to: &Path) -> std::io::Result<()> {
    if !from.exists() {
        panic!("The source path does not exist");
    } else if from.is_file() {
        if to.exists() && to.is_dir() {
            println!(
                "Error: The file {} already exists as a directory in the cache folder",
                to.display()
            );
            exit(1);
        }
        std::fs::copy(from, to)?;
    } else {
        // is_dir() => true
        if to.exists() {
            if to.is_file() {
                println!(
                    "Error: The directory {} already exists as a file in the cache folder",
                    to.display()
                );
                exit(1);
            }
        } else {
            std::fs::create_dir(to)?;
        }

        // READ DIR FILES
        let paths = fs::read_dir(from)?;

        for path in paths {
            let base = path.unwrap();
            let new_from = from.join(base.file_name());
            let new_to = to.join(base.file_name());
            copy_rec(&new_from, &new_to)?;
        }
    }

    Ok(())
}
