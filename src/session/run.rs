// use std::fs;
use std::path::Path;
use std::process::{exit, Command, Stdio};

#[path = "../lib/mod.rs"]
mod lib;
use lib::common::{
    confirm_action, ensure_session_paths, get_paths_for_session, is_valid_name, DX11Paths,
};
use lib::config::{read_config, write_config, DX11Config};

const DOCKER_USER: &str = "user";
const CAPS_BASE: &str = "--cap-add=SYS_MODULE --cap-add=SYS_RAWIO --cap-add=SYS_PACCT --cap-add=SYS_ADMIN --cap-add=SYS_NICE --cap-add=SYS_RESOURCE --cap-add=SYS_TIME --cap-add=AUDIT_CONTROL --cap-add=SYSLOG --cap-add=DAC_READ_SEARCH --cap-add=LINUX_IMMUTABLE --cap-add=IPC_LOCK --cap-add=IPC_OWNER --cap-add=SYS_PTRACE --cap-add=LEASE --cap-add=AUDIT_READ";
const USE_SYSTEMD: bool = false;

const GET_HOST_IP_CMD: &str =
    "ip -4 addr show scope global dev docker0 | grep inet | awk '{print $2}' | cut -d / -f 1";
const HOST_DOMAIN: &str = "host.local";

const CHECK_TOR_CMD: &str = "sudo docker container ls | grep tor-router";
const LAUNCH_TOR_CMD: &str = "sudo docker run -d --rm --name tor-router --cap-add NET_ADMIN --dns 127.0.0.1 flungo/tor-router";

pub fn run_session(name: &str) {
    if !is_valid_name(name.to_string()) {
        println!("The session name \"{}\" is not valid", name);
        exit(1);
    }

    let paths = get_paths_for_session(name.to_string());

    if !paths.config_file.exists() {
        println!("Error: The session config file does not exist");
        exit(1);
    }

    let config = match read_config(&paths.config_file) {
        Err(err) => {
            println!("Error: Could not read the config file ({})", err);
            exit(1);
        }
        Ok(value) => value,
    };

    // TODO: CHECK CURRENT SETUP

    // Check that we are not already running the same session
    let pid = std::process::id();

    let mut docker_build = Command::new("sh");
    docker_build.arg("-c");
    docker_build.arg(&format!(
        "ps u | grep 'dx11\\s*run\\s*{}' | grep -v grep | grep -v {}",
        name, pid
    ));

    docker_build.stdin(Stdio::null());
    docker_build.stdout(Stdio::null());
    docker_build.stderr(Stdio::null());
    match docker_build.status().unwrap().code() {
        Some(status) => {
            if status == 0 {
                println!("You are already running {}", &name);
                exit(1);
            }
        }
        _ => {}
    }

    // TODO: CHECK ENVIRONMENT AND DEPENDENCIES

    // Prepare the parameters
    let docker_image_name = format!("dx11-{}", name);
    let systemd_args = if USE_SYSTEMD {
        "--init=systemd --dbus=system"
    } else {
        ""
    };
    let home_origin = paths.home_src_data;
    let caps_args = CAPS_BASE;
    let tor_args = "";
    let host_bind_args = "";

    // Keep track of the current VT
    let vt_output = Command::new("fgconsole").output().unwrap();
    let is_on_x11 = match vt_output.status.code() {
        Some(code) => code != 0,
        None => {
            println!("Error: Failed to check the current VT");
            exit(1);
        }
    };
    let currentVtNumber: u8;
    let sessionVtNumber: u8;

    if is_on_x11 {
        // retry as root
        let vt_output = Command::new("sudo").arg("fgconsole").output().unwrap();
        // TODO: COMPLETE
    } else {
        // TODO: Check trailing "\n"
        let num = std::str::from_utf8(&vt_output.stdout).unwrap();
        currentVtNumber = num.parse().expect("Error: The current VT is not valid");
        sessionVtNumber = currentVtNumber;
    }

    // WIP

    //     let cached_docker_file_path = paths.docker_folder.join("Dockerfile");
    //     let build_command = "sudo";
    //     let build_command_args = [
    //         "docker",
    //         "build",
    //         "-t",
    //         &docker_image_name,
    //         "-f",
    //         &cached_docker_file_path.display().to_string(),
    //         &paths.docker_folder.display().to_string(),
    //     ];

    //     println!("Building the Docker image {}", &docker_image_name);

    //     let mut docker_build = Command::new(build_command);

    //     docker_build.stdin(Stdio::null());
    //     docker_build.stdout(Stdio::inherit());
    //     docker_build.stderr(Stdio::inherit());

    //     for arg in build_command_args.iter() {
    //         docker_build.arg(arg);
    //     }
    //     match docker_build.output() {
    //         Err(err) => {
    //             println!("Error: Failed to build the image ({})", err);
    //             exit(1);
    //         }
    //         Ok(_) => println!("Done"),
    //     };
}

// Check if TOR is running and start it otherwise
// Quits if it can't be started
fn startTor() {
    // let checkOk = execShellCmd(CHECK_TOR_CMD)
    // if checkOk != 0:
    //     echo "Starting TOR"
    //     if execShellCmd(LAUNCH_TOR_CMD) != 0:
    //         raise newException(Exception, "Can't start the TOR network")
}
fn stopTor() {
    // echo "Stopping TOR"
    // let status = execShellCmd("sudo docker container ls | grep flungo/tor-router | awk '{print $1}' | xargs sudo docker container stop")
    // if status != 0: echo "FAILED"
}

// Mounts the encrypted folder using ecryptfs
// Quits if it fails
fn mountEncryptedHome(sessionPaths: DX11Paths) {
    // if execShellCmd("mount | grep \"^" & sessionPaths.homeSrcData &
    //         "\" > /dev/null") != 0:
    //     # mount
    //     let mountCommand = fmt"sudo -u {DOCKER_USER} ecryptfs-simple -a -c $HOME/.ecryptfs.config {sessionPaths.homeSrcData} {sessionPaths.homeEncryptedMount}"
    //     if execShellCmd(mountCommand) != 0:
    //         raise newException(Exception, "Unable to mount the encrypted home folder")
}
fn umountEncryptedHome(sessionPaths: DX11Paths) {
    // echo "Unmounting the encrypted folder"
    // let status = execShellCmd(fmt"sudo -u {DOCKER_USER} ecryptfs-simple -u {sessionPaths.homeSrcData}")
    // if status != 0: echo "FAILED"
}
