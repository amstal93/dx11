use regex::Regex;
use std::env;
use std::fs;
use std::io;
use std::path::{Path, PathBuf};

pub struct DX11Paths {
    pub base_path: PathBuf,
    pub config_file: PathBuf,
    pub home_src_data: PathBuf,
    pub home_encrypted_mount: PathBuf, //used only when encryption is enabled
    pub shared_folder: PathBuf,
    pub docker_folder: PathBuf,
}

pub fn get_env(key: &str) -> String {
    env::var(key).unwrap_or("".to_string())
}

pub fn get_dx11_data_path() -> PathBuf {
    let home_path = env::var("HOME").expect("Unable to read the HOME folder");
    Path::new(&home_path)
        .join(".local")
        .join("share")
        .join("dx11")
}

pub fn get_paths_for_session(name: String) -> DX11Paths {
    let base_path = get_dx11_data_path().join(&name);
    let config_file = base_path.join("config.json");
    let home_src_folder = base_path.join("home-src");
    let home_encrypted_mount = base_path.join("home-mount");
    let share_folder = base_path.join("Public").join("DX11");
    let docker_folder = base_path.join("docker");

    let config = DX11Paths {
        base_path: base_path,
        config_file: config_file,
        home_src_data: home_src_folder,
        home_encrypted_mount: home_encrypted_mount,
        shared_folder: share_folder,
        docker_folder: docker_folder,
    };
    config
}

pub fn ensure_session_paths(name: String) -> std::io::Result<()> {
    let config = get_paths_for_session(name);

    if !Path::new(&config.base_path).is_dir() {
        fs::create_dir_all(config.base_path).expect("Failed to create the session data folder");
    } else if Path::new(&config.config_file).is_dir() {
        println!("The config file can't be a directory");
        std::process::exit(1);
    }

    let src_data = Path::new(&config.home_src_data);
    if src_data.is_file() {
        println!("The home-src path can't be a file");
        std::process::exit(1);
    } else if !src_data.is_dir() {
        fs::create_dir_all(config.home_src_data).expect("Failed to create the home-src folder");
    }

    let encrypted_mount = Path::new(&config.home_encrypted_mount);
    if encrypted_mount.is_file() {
        println!("The home-mount path can't be a file");
        std::process::exit(1);
    } else if !encrypted_mount.is_dir() {
        fs::create_dir_all(config.home_encrypted_mount)
            .expect("Failed to create the home-mount folder");
    }

    let shared_folder = Path::new(&config.shared_folder);
    if shared_folder.is_file() {
        println!("The shared path can't be a file");
        std::process::exit(1);
    } else if !shared_folder.is_dir() {
        fs::create_dir_all(config.shared_folder).expect("Failed to create the shared folder");
    }

    let docker_folder = Path::new(&config.docker_folder);
    if docker_folder.is_file() {
        println!("The docker destination path can't be a file");
        std::process::exit(1);
    } else if !docker_folder.is_dir() {
        fs::create_dir_all(config.docker_folder).expect("Failed to create the docker folder");
    }
    Ok(())
}

pub fn is_valid_name(name: String) -> bool {
    let re = Regex::new(r"^[a-zA-Z][a-zA-Z0-9.\-]*$").unwrap();
    let caps = re.captures(&name).unwrap();
    match caps.get(0) {
        Some(_) => true,
        _ => false,
    }
}

pub fn confirm_action(text: &str, default_value: bool) -> bool {
    println!(
        "{} {} ",
        text,
        if default_value { "(Y/n)" } else { "(y/N)" }
    );

    let mut input = String::new();
    io::stdin()
        .read_line(&mut input)
        .expect("Could not read the response");

    match input.as_str().trim() {
        "y" | "Y" | "yes" | "Yes" | "YES" => true,
        "n" | "N" | "no" | "No" | "NO" => false,
        _ => default_value,
    }
}
