use serde::{Deserialize, Serialize};
use std::fs;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct DX11Config {
    pub useTor: bool,
    pub encryptHome: bool,
}

impl DX11Config {
    pub fn new(encryptHome: bool, useTor: bool) -> DX11Config {
        DX11Config {
            encryptHome: encryptHome,
            useTor: useTor,
        }
    }
}

pub fn read_config(config_file: &Path) -> std::io::Result<DX11Config> {
    let mut str_contents = String::new();
    let mut file = File::open(config_file)?;
    file.read_to_string(&mut str_contents)?;
    let deserialized: DX11Config = serde_json::from_str(&str_contents)?;
    Ok(deserialized)
}

pub fn write_config(config: &DX11Config, config_file: &Path) -> std::io::Result<()> {
    fs::create_dir_all(config_file.parent().unwrap())?;
    let str_config = serde_json::to_string(&config).unwrap();
    let display = config_file.display();
    let mut file = File::create(&config_file)?;
    file.write_all(str_config.as_bytes())?;
    Ok(())
}
